#!/bin/bash

function run {
  if ! pgrep -f $1 ;
  then
    $@&
  fi
}

# Configure monitor layout for desktop.
run xrandr --output eDP1 --off --output DP1 --off --output DP1-1 --primary --mode 2560x1440 --pos 0x0 --rotate normal --output DP1-2 --mode 2560x1440 --pos 2560x0 --rotate normal --output DP1-3 --off --output DP2 --off --output DP3 --off --output VIRTUAL1 --off
# Configure monitor latout for laptop.
#run xrandr --output eDP1 --primary --mode 3840x2160 --pos 0x0 --rotate normal --output DP1 --off --output DP2 --off --output DP3 --off --output VIRTUAL1 --off

# Start compositor for transparency.
run picom &

# Restore wallpaper.
#run nitrogen --restore

# Detect and change screen setup.
#run autorandr --change
