# Author: hampusg
#
# ~/.bashrc
#

# If not running interactively, don't do anything.
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias cp='cp -v'
alias dotfiles='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
alias visudo='sudo EDITOR=vim visudo'
alias vi='vim'

export VISUAL=vim
export EDITOR="$VISUAL"

PS1='\e[33m\][\t] \e[34m\]\u\e[94m\]@\e[34m\]\h: \e[94m\w\] \e[0m\] \n$ '
