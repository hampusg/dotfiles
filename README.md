## Setup on New Machine

```
cd ~
git clone --bare https://gitlab.com/hampusg/dotfiles.git $HOME/.dotfiles
git --git-dir=$HOME/.dotfiles --work-tree=$HOME checkout
source .bashrc
dotfiles config --local status.showUntrackedFiles no
```

You may need to remove (or backup)  existing files that clash.

See: https://www.atlassian.com/git/tutorials/dotfiles

## License

All files and scripts in this repository are licensed under the [MIT License](./LICENSE).
