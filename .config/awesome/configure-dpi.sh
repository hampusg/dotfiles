#!/bin/bash

function run {
  if ! pgrep -f $1 ;
  then
    $@&
  fi
}

function configure_laptop() {
  # Start Xorg server at this DPI
  #xrandr --dpi 192
  # Merge & load configuration from .Xresources
  #xrdb -merge ~/.Xresources

  # Let QT autodetect dpi and scaling.
  #export QT_AUTO_SCREEN_SCALE_FACTOR=1
  #export QT_ENABLE_HIGHDPI_SCALING=1 # Replaces QT_AUTO_SCREEN_SCALE_FACTOR in Qt 5.14.
  # Global QT scale (including fonts).
  #export QT_SCALE_FACTOR=2
  #export QT_SCREEN_SCALE_FACTORS=2
  #export QT_FONT_DPI=192

  #defaults write NSGlobalDomain GSScaleFactor 1.5???

  # Set GDK scaling.
  #export GDK_SCALE=2
  #export GDK_DPI_SCALE=0.5

  
  xrandr --output eDP1 --primary --mode 3840x2160 --pos 0x0 --rotate normal --output DP1 --off --output DP1-1 --off --output DP1-2 --off --output DP1-3 --off --output DP2 --off --output DP3 --off --output VIRTUAL1 --off
  xrandr --output eDP1 --scale 0.5x0.5
}

function configure_docked_office() {
  # Start Xorg server at this DPI
  #xrandr --dpi 96
  # Merge & load configuration from .Xresources
  #xrdb -merge ~/.Xresources

  # Let QT autodetect dpi and scaling.
  #export QT_AUTO_SCREEN_SCALE_FACTOR=1

  # Set GDK scaling.
  #export GDK_SCALE=1

  #xrandr --output eDP1 --scale 1.0x1.0

  xrandr --output eDP1 --off --output DP1 --off --output DP1-1 --primary --mode 2560x1440 --pos 0x0 --rotate normal --output DP1-2 --mode 2560x1440 --pos 2560x0 --rotate normal --output DP1-3 --off --output DP2 --off --output DP3 --off --output VIRTUAL1 --off
}

#current_screen_arrangement="$(autorandr --current)"
#ARRANGEMENT_DOCK_OFFICE="dock-office"
#ARRANGEMENT_LAPTOP="laptop"

#echo "${current_screen_arrangement}"

if xrandr --listactivemonitors | grep eDP1
then
  configure_docked_office
else
  configure_laptop
fi

#case "$current_screen_arrangement" in
#  $ARRANGEMENT_DOCK_OFFICE)
#    configure_docked_office
#    ;;
#  $ARRANGEMENT_LAPTOP)
#    configure_laptop
#    ;;
#esac  
